import React from 'react';
import '../styles/Card.css'

class Card extends React.Component {
    
    render() {
    
        const {pharmacy,onDelete,onFormUpdate, isGarde} = this.props
        

        
            return(
              
                <div className="menu d-flex justify-content-center">
                    <div className="card carte">
                        <div className="card-body">
                            <h2 className="card-title nom">{pharmacy.nom}</h2>
                            <h3 className="card-subtitle mb-2">De garde le : {pharmacy.garde}</h3>
                            <p className="card-text">adresse : {pharmacy.quartier} à {pharmacy.ville}</p>
                            {!isGarde && <button type="button" onClick={() => onFormUpdate(pharmacy)} className="btn btn-info bouton boutonModif">Modifier</button>}
                            {!isGarde && <button type="button" onClick={() => onDelete(pharmacy.id)} className="btn btn-danger bouton boutonSuppr">Supprimer</button>}
                        </div>
                    </div>
                </div>
            )

        

        
          
      
    }
}
    
    
    

export default Card;