import React from 'react';
import '../styles/Footer.css';


class Footer extends React.Component {
    render() {
        const {onPharma} = this.props;
        return (
            <div className="footer container-fluid">
                <div className="col ">
                    <ul>
                        <li><button className="lien" onClick={() => onPharma()}>Accueil</button></li>
                    </ul>
                </div>
                <div className="col">
                    <p className="texte">By Marvin</p>
                </div>
            </div>
        )
    }
}

export default Footer