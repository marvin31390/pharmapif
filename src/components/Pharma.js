import React, {Component} from 'react';
import axios from 'axios';
import Card from './Card';
import FormUpdate from './FormUpdate';


class Pharma extends Component {
    state = {
        pharmacyList: [],
        currentPharmacy: {},
        formUpdate: false
      }
      
      componentDidMount() {
        axios.get(`https://cherry-tart-07032.herokuapp.com/pharma`)
          .then(res => {
            const data = res.data;
            this.setState({pharmacyList: data});
          })
      } 
    
      handleSubmit = (modifiedPharm) => {  
        axios.put(`https://cherry-tart-07032.herokuapp.com/pharma/${modifiedPharm.id}`, modifiedPharm)
          .then(res => {
            const tempPharmList = this.state.pharmacyList.map((pharmacy) => {
              if(pharmacy.id === modifiedPharm.id){
                return modifiedPharm
              }
              return pharmacy
            })
            this.setState({pharmacyList: tempPharmList})
            this.setState({formUpdate: false});
          })
      }

      formUpdate (pharmacy) {
        this.setState({currentPharmacy: pharmacy})
        this.setState({formUpdate: !this.state.formUpdate});
        
      }  
      formClosed () {
        this.setState({formUpdate: false});
        console.log(this.state.formUpdate)
      }

    delete (id) {
      axios.delete(`https://cherry-tart-07032.herokuapp.com/pharma/${id}`)
      .then(res => {
        const pharmacyList = this.state.pharmacyList.filter(item => item.id !== id);
        this.setState({ pharmacyList });
      })
    }

  
      
   
      render() {
        
        const {pharmacyList, currentPharmacy} = this.state;

        return (
          <>
            {this.state.formUpdate && <FormUpdate currentPharmacy={currentPharmacy} onFormClosed={() => this.formClosed()} onHandleSubmit={this.handleSubmit}/>}
            {   
              pharmacyList.length && pharmacyList.map((pharmacy, index ) => {
              return (
                <Card key={index} onDelete={(id) => this.delete(id)} onFormUpdate={(pharmacy) => this.formUpdate(pharmacy)} pharmacy={pharmacy}/>)
              })
            }
          </>
      )
        }
      }

    
    
    

export default Pharma;