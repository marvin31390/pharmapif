import React from 'react';
import axios from 'axios';
import '../styles/Form.css';


class Form extends React.Component {

    state = {
        
        send: false
    }

    handleChange = event => {
        console.log(this.state)
        const type = event.target.type;
        const name = event.target.name;
        if (type === 'text'){
            if(name === 'nom'){
                this.setState({ name: event.target.value});
            }else if (name === 'quartier') {
                this.setState({ quartier: event.target.value});
            }else if (name === 'ville'){
                this.setState({ city: event.target.value});
            }
        }else {
            this.setState({ garde: event.target.value});
        }
        
        
      }

    handleSubmit = (e) => {
        e.preventDefault();
        const data = {
          nom: this.state.name,
          quartier: this.state.quartier,
          ville: this.state.city,
          garde: this.state.garde
        };
        e.target[0].value = '';
        e.target[1].value = '';
        e.target[2].value = '';
        e.target[3].value = '';
        

        
    
        axios.post(`https://cherry-tart-07032.herokuapp.com/pharma`,  data )
          .then(res => {
            this.setState({send: true})
          })
      }

    render () {
        let message = 'Vous avez ajouter une pharmacie !'
        return(
            <form className="formulaire" onSubmit={this.handleSubmit}>
                <div className="col">
                    <label>Nom de la pharmacie
                        <input type="text" className="input-form form-control" name="nom" onChange={this.handleChange} placeholder="Nom de la pharmacie" aria-label="nom" required></input>
                    </label>
                </div>
                <div className="col">
                    <label>Quartier 
                        <input type="text" className="input-form form-control" name="quartier" onChange={this.handleChange} placeholder="Quartier" aria-label="quartier" required></input>
                    </label>
                </div>
                <div className="col">
                    <label>Ville
                        <input type="text" className="input-form form-control" name="ville"  onChange={this.handleChange} placeholder="Ville exemple Paris" aria-label="ville" required></input>
                    </label>
                </div>
                <div className="col">
                    <select className="form-select input-form" name="garde" onChange={this.handleChange} aria-label="Default select example" required>
                        <option>Jour de garde</option>
                        <option value="lundi">Lundi</option>
                        <option value="mardi">Mardi</option>
                        <option value="mercredi">Mercredi</option>
                        <option value="jeudi">Jeudi</option>
                        <option value="vendredi">Vendredi</option>
                        <option value="samedi">Samedi</option>
                        <option value="dimanche">Dimanche</option>
                    </select>
                </div>
                <button type="submit"  className="btn btn-success bouton">Ajouter</button>
                {this.state.send && <p>{message}</p>}
            </form>
        )
    }
}


export default Form;