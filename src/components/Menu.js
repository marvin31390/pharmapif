import React from 'react';
import logo from '../img/logo.svg';
import '../styles/Menu.css';




class Menu extends React.Component {
    
    render() {
        const {garde, formActive, pharma} = this.props;
        return (
                <div className="title">
                    <h1>Pharmapi</h1>
                    <div className="container-fluid">
                        <img src={logo} className="App-logo" alt="pharmacie" width="250" height="250"></img>
                        <div className="boutton-accueil">
                            <button className="button button-home" onClick={() => pharma()} >Accueil</button>
                            <button className="button button-home" onClick={() => garde()}>De Garde</button>
                            <button className="button button-home" onClick={() => formActive()}>Ajouter une pharmacie</button>
                        </div>
                    </div>
                </div>
        )
    }
}


export default Menu;





