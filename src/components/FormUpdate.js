import React from 'react';
import '../styles/Form.css';


class FormUpdate extends React.Component {
    
    state = {
        id: '',
        nom: '',
        quartier: '',
        ville: '',
        garde: '',
        
    }
    
    
    handleChange = event => {
        
        const type = event.target.type;
        const name = event.target.name;
        if (type === 'text'){
            if(name === 'nom'){
                this.setState({ nom: event.target.value});
            }else if (name === 'quartier') {
                this.setState({ quartier: event.target.value});
            }else if (name === 'ville'){
                this.setState({ ville: event.target.value});
            }
        }else {
            this.setState({ garde: event.target.value});
        }
        
        
      }


    componentDidMount() {
        this.setState(this.props.currentPharmacy)
    }
   

    render () {
        this.myRef = React.createRef();
       
        const pharmacy = this.state;
        return(
            <form className="formulaire">
                <div className="col">
                    <label>Nom de la pharmacie
                        <input type="text" className="input-form form-control" name="nom" value={pharmacy.nom} onChange={this.handleChange} placeholder="Nom de la pharmacie" aria-label="nom" required></input>
                    </label>
                </div>
                <div className="col">
                    <label>Quartier 
                        <input type="text" className="input-form form-control" value={pharmacy.quartier} name="quartier" onChange={this.handleChange} placeholder="Quartier" aria-label="quartier" required></input>
                    </label>
                </div>
                <div className="col">
                    <label>Ville
                        <input type="text" className="input-form form-control" name="ville" value={pharmacy.ville}  onChange={this.handleChange} placeholder="Ville exemple Paris" aria-label="ville" required></input>
                    </label>
                </div>
                <div className="col">
                    <select className="form-select input-form" name="garde" value={pharmacy.garde} onChange={this.handleChange} aria-label="Default select example" required>
                        <option>Jour de garde</option>
                        <option value="lundi">Lundi</option>
                        <option value="mardi">Mardi</option>
                        <option value="mercredi">Mercredi</option>
                        <option value="jeudi">Jeudi</option>
                        <option value="vendredi">Vendredi</option>
                        <option value="samedi">Samedi</option>
                        <option value="dimanche">Dimanche</option>
                    </select>
                </div>
                <button type="button" onClick={() => this.props.onHandleSubmit(this.state)} className="btn btn-success bouton">Ajouter</button>
                <button type="button" onClick={() => this.props.onFormClosed()} className="btn btn-danger bouton">Annuler</button>

            </form>
        )
    }
}


export default FormUpdate;