import React, {Component} from 'react';
import axios from 'axios';
import Card from './Card';

class Garde extends Component {
    state = {
        pharmacyList: []
      }
      
      componentDidMount() {
        axios.get(`https://cherry-tart-07032.herokuapp.com/pharma-garde`)
          .then(res => {
            const data = res.data;
            this.setState({pharmacyList: data});
          })
      }
    
    
      
    
      render() {
        const {pharmacyList} = this.state;
        return (
          <>
      
        
        
    
        {   
              pharmacyList.length && pharmacyList.map((pharmacy, index ) => {
              return <Card key={index} pharmacy={pharmacy} isGarde />
          })
        }
      </>
      )
      }
}
    
    
    

export default Garde;